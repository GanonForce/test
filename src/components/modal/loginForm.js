import React from 'react';
import { Form, Input, Button } from 'antd'

class LoginForm extends React.Component {

  state = {
    ResetPassword: true
  }

  render() {
    return (
      <Form>
        {this.state.ResetPassword ?
        <div>
          <Form.Item>
            <Input type={'text'} placeholder={'Nickname'}/>
          </Form.Item>
          <Form.Item>
            <Input type={'password'} placeholder={'Password'}/>
            <a onClick={() => this.setState({  ResetPassword: false })}>reset</a>
          </Form.Item>
          <Button style={{ width: '100%' }} htmlType={'submit'} type={'primary'}>Login</Button>
        </div>:
        <div>
          <Form.Item>
            <Input type={'text'} placeholder={'Nickname'}/>
             <a onClick={() => this.setState({  ResetPassword: true })}>login</a>
          </Form.Item>
             <Button style={{ width: '100%' }} htmlType={'submit'} type={'primary'}>Reset</Button>
        </div>
        }
      </Form>
    )
  }
};

export default Form.create()(LoginForm)