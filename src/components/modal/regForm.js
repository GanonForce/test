import React from 'react';
import { Form, Input, Button } from 'antd'

class RegForm extends React.Component {

  render() {
    return (
      <Form>
        <Form.Item>
          <Input type={'text'} placeholder={'Nickname'}/>
        </Form.Item>
        <Form.Item>
          <Input type={'password'} placeholder={'Password'}/>
        </Form.Item>
        <Form.Item>
          <Input type={'password'} placeholder={'Repeat password'}/>
        </Form.Item>
        <Button style={{ width: '100%' }} htmlType={'submit'} type={'primary'}>Register</Button>
      </Form>
    )
  }
};

export default Form.create()(RegForm)