import React from 'react';
import { Button, Modal} from 'antd'

import LoginForm from './loginForm'
import RegForm from './regForm'

export default class ModalWindow extends React.Component {

  state = {
    ModalVisible: false,
    IsLoginForm: true
  }

  render() {
    return (

    <div>
      <Button type={'primary'} onClick={() => this.setState({ ModalVisible: true })} >LOG IN / SIGN UP</Button>
      <Modal
        footer={null}
        visible={this.state.ModalVisible}
        onCancel={() => this.setState({ ModalVisible: false })}>

        {this.state.IsLoginForm ?
          <div style={{marginTop: 20}}>
            <LoginForm />
            <a onClick={() => this.setState({ IsLoginForm: false })}>register</a>
          </div>:
          <div>
            <RegForm />
            <a onClick={() => this.setState({ IsLoginForm: true })}>sign up</a>
          </div> 
        } 
      </Modal>
    </div>
    )
  }
};
