import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom'
import ModalWindow from './components/modal'

import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className='App'>
          <ModalWindow />
        </div>
      </Router>
    );
  }
}

export default App;
